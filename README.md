# car

fuel recorder for my car

# installation

```bash
gradlew clean build
```

copy the corresponding jar file from `build/libs` to the folder of your destination

# running the app

create your own `application.properties` file and put it on your server; call that file on runtime

```bash
java -Dspring.config.location=/wherever/you/have/it/application.properties -jar car-VERSION.jar 
```
