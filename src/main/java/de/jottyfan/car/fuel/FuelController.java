package de.jottyfan.car.fuel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import de.jottyfan.car.fuel.model.FuelBean;
import jakarta.annotation.security.RolesAllowed;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class FuelController {
	private static final Logger LOGGER = LogManager.getLogger(FuelController.class);
	
	@Autowired
	private FuelService fuelService;

	private List<FuelBean> cachedFuels;
	
	@GetMapping("/logout")
	public String getLogout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
	}
	
	@RolesAllowed("car_user")
	@GetMapping("/secure/welcome")
	public String getWelcome() {
		return "secure/welcome";
	}
	
	@GetMapping("/")
	public String getIndex() {
		this.cachedFuels = null;
		return "public/index";
	}

	@RolesAllowed("car_user")
	@GetMapping("/secure/to_table")
	public String getTable() {
		this.cachedFuels = null;
		return "secure/table";
	}

	@RolesAllowed("car_user")
	@RequestMapping(value = "/secure/to_bean", method = RequestMethod.GET)
	public String getBean(Model model) {
		FuelBean bean = model.containsAttribute("fuelBean") ? (FuelBean) model.getAttribute("fuelBean") : new FuelBean();
		model.addAttribute("fuelBean", bean);
		return "secure/bean";
	}
	
	@RolesAllowed("car_user")
	@RequestMapping(value = "/secure/to_existing_bean/{fkFuelBean}", method = RequestMethod.GET)
	public String getBean(Model model, @PathVariable Integer fkFuelBean) throws Exception {
		FuelBean bean = getFuelBean(fkFuelBean);
		model.addAttribute("fuelBean", bean);
		return "secure/bean";
	}

	private FuelBean getFuelBean(Integer fkFuelBean) throws Exception {
		for (FuelBean bean : getFuels()) {
			if (bean.getPk().equals(fkFuelBean)) {
				return bean;
			}
		}
		throw new Exception("bean not found");
	}

	@RolesAllowed("car_user")
	@RequestMapping(value = "/secure/do_upsert", method = RequestMethod.POST)
	public String getUpsert(Model model, @ModelAttribute FuelBean fuelBean) {
		Integer affected = fuelService.upsert(fuelBean);
		model.addAttribute("fuelBean", fuelBean);
		LOGGER.info("affected rows: {}", affected);
		return affected > 0 ? getTable() : getBean(model);
	}

	@RolesAllowed("car_user")
	@GetMapping("/secure/jsonfuels")
	@ResponseBody
	public List<FuelBean> getJsonFuels() {
		List<FuelBean> fuels = fuelService.findAll();
		return fuels;
	}

	@ModelAttribute("fuels")
	public List<FuelBean> getFuels() {
		if (cachedFuels == null || cachedFuels.size() < 1) {
			cachedFuels = fuelService.findAll();
		}
		return cachedFuels;
	}
	
	@ModelAttribute("averagelkm")
	@ResponseBody
	public BigDecimal getAverageLiterPerKm() {
		List<FuelBean> fuels = getFuels();
		Integer minMileage = 1000000; // my car won't even reach this milestone :)
		Integer maxMileage = 0;
		BigDecimal summedAmount = new BigDecimal(0);
		for (FuelBean bean : fuels) {
			summedAmount = summedAmount.add(bean.getAmount());
			minMileage = bean.getMileage() < minMileage ? bean.getMileage() : minMileage;
			maxMileage = bean.getMileage() > maxMileage ? bean.getMileage() : maxMileage;
		}
		BigDecimal totalMileage = new BigDecimal(maxMileage - minMileage);
		BigDecimal calculated = totalMileage.intValue() != 0
				? new BigDecimal((summedAmount.doubleValue() / totalMileage.doubleValue()) * 100d)
				: new BigDecimal(0);
		return calculated;
	}

	@ModelAttribute("averageel")
	@ResponseBody
	public BigDecimal getAverageEuroPerLiter() {
		List<FuelBean> fuels = getFuels();
		BigDecimal summedLiter = new BigDecimal(0);
		BigDecimal summedEuro = new BigDecimal(0);
		for (FuelBean bean : fuels) {
			summedLiter = summedLiter.add(bean.getAmount());
			summedEuro = summedEuro.add(bean.getPrice());
		}
		BigDecimal calculated = summedLiter.intValue() != 0
				? new BigDecimal(summedEuro.doubleValue() / summedLiter.doubleValue())
				: new BigDecimal(0);
		return calculated;
	}

	@ModelAttribute("chartjsdata")
	@ResponseBody
	public List<BigDecimal> getChartjsData() {
		List<FuelBean> fuels = getFuels();
		List<BigDecimal> list = new ArrayList<>();
		for (FuelBean bean : fuels) {
			list.add(bean.getEuro_per_l());
		}
		return list;
	}

	@ModelAttribute("chartjslabel")
	@ResponseBody
	public List<Integer> getChartjsLabel() {
		List<FuelBean> fuels = getFuels();
		List<Integer> list = new ArrayList<>();
		for (FuelBean bean : fuels) {
			list.add(bean.getMileage());
		}
		return list;
	}
}
