package de.jottyfan.car.fuel;

import static de.jottyfan.car.db.jooq.Tables.T_MILEAGE;
import static de.jottyfan.car.db.jooq.Tables.V_MILEAGE;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.InsertValuesStep8;
import org.jooq.Record;
import org.jooq.SelectSeekStep1;
import org.jooq.UpdateConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.car.db.jooq.tables.records.TMileageRecord;
import de.jottyfan.car.db.jooq.tables.records.VMileageRecord;
import de.jottyfan.car.fuel.model.FuelBean;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class FuelRepository {
	private static final Logger LOGGER = LogManager.getLogger(FuelRepository.class);

	@Autowired
	private DSLContext jooq;

	/**
	 * get all records from v_mileage
	 * 
	 * @return the mileage records
	 */
	public List<FuelBean> getMileages() {
		SelectSeekStep1<VMileageRecord, Integer> sql = jooq.selectFrom(V_MILEAGE).orderBy(V_MILEAGE.MILEAGE);
		LOGGER.trace("{}", sql.toString());
		List<FuelBean> list = new ArrayList<>();
		for (Record r : sql.fetch()) {
			Integer pk = r.get(V_MILEAGE.PK);
			BigDecimal amount = r.get(V_MILEAGE.AMOUNT);
			LocalDateTime buydate = r.get(V_MILEAGE.BUYDATE);
			String fuel = r.get(V_MILEAGE.FUEL);
			String location = r.get(V_MILEAGE.LOCATION);
			Integer mileage = r.get(V_MILEAGE.MILEAGE);
			BigDecimal price = r.get(V_MILEAGE.PRICE);
			String provider = r.get(V_MILEAGE.PROVIDER);
			String annotation = r.get(V_MILEAGE.ANNOTATION);
			BigDecimal euro_per_l = r.get(V_MILEAGE.EURO_2fL);
			list.add(new FuelBean(pk, amount, buydate, fuel, location, mileage, price, provider, annotation, euro_per_l));
		}
		return list;
	}

	/**
	 * upsert fuel bean
	 * 
	 * @param bean the bean
	 * @return the number of affected database rows, should be 1
	 */
	public Integer upsertMileage(FuelBean bean) {
		if (bean.getPk() == null) {
			InsertValuesStep8<TMileageRecord, Integer, BigDecimal, String, LocalDateTime, String, String, BigDecimal, String> sql = jooq
					.insertInto(T_MILEAGE, T_MILEAGE.MILEAGE, T_MILEAGE.AMOUNT, T_MILEAGE.ANNOTATION, T_MILEAGE.BUYDATE,
							T_MILEAGE.FUEL, T_MILEAGE.LOCATION, T_MILEAGE.PRICE, T_MILEAGE.PROVIDER)
					.values(bean.getMileage(), bean.getAmount(), bean.getAnnotation(), bean.getBuydate(), bean.getFuel(),
							bean.getLocation(), bean.getPrice(), bean.getProvider());
			LOGGER.trace("{}", sql.toString());
			return sql.execute();
		} else {
			UpdateConditionStep<TMileageRecord> sql = jooq.update(T_MILEAGE)
					.set(T_MILEAGE.MILEAGE, bean.getMileage())
					.set(T_MILEAGE.ANNOTATION, bean.getAnnotation())
					.set(T_MILEAGE.BUYDATE, bean.getBuydate()).set(T_MILEAGE.FUEL, bean.getFuel())
					.set(T_MILEAGE.LOCATION, bean.getLocation()).set(T_MILEAGE.PRICE, bean.getPrice())
					.set(T_MILEAGE.PROVIDER, bean.getProvider()).where(T_MILEAGE.PK.eq(bean.getPk()));
			LOGGER.trace("{}", sql.toString());
			return sql.execute();
		}
	}
}
