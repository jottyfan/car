package de.jottyfan.car.fuel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.jottyfan.car.fuel.model.FuelBean;

/**
 * 
 * @author henkej
 *
 */
@Service
@Transactional(transactionManager = "transactionManager")
public class FuelService {

	@Autowired
	private FuelRepository repository;

	public List<FuelBean> findAll() {
		return repository.getMileages();
	}

	public Integer upsert(FuelBean fuelBean) {
		return repository.upsertMileage(fuelBean);
	}
}
