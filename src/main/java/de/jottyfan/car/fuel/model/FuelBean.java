package de.jottyfan.car.fuel.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

/**
 * 
 * @author henkej
 *
 */
public class FuelBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;

	@NumberFormat(style = NumberFormat.Style.NUMBER)
	private BigDecimal amount;

	@DateTimeFormat
	private LocalDateTime buydate;

	private String fuel;
	private String location;
	private Integer mileage;

	@NumberFormat(style = NumberFormat.Style.CURRENCY)
	private BigDecimal price;

	private String provider;
	private String annotation;
	private BigDecimal euro_per_l;

	public FuelBean() {
		this.pk = null;
	}

	public FuelBean(Integer pk, BigDecimal amount, LocalDateTime buydate, String fuel, String location, Integer mileage,
			BigDecimal price, String provider, String annotation, BigDecimal euro_per_l) {
		super();
		this.pk = pk;
		this.amount = amount;
		this.buydate = buydate;
		this.fuel = fuel;
		this.location = location;
		this.mileage = mileage;
		this.price = price;
		this.provider = provider;
		this.annotation = annotation;
		this.euro_per_l = euro_per_l;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("FuelBean@{pk=").append(pk);
		buf.append(", amount=").append(amount);
		buf.append(", buydate=").append(buydate);
		buf.append(", fuel=").append(fuel);
		buf.append(", location=").append(location);
		buf.append(", mileage=").append(mileage);
		buf.append(", price=").append(price);
		buf.append(", provider=").append(provider);
		buf.append(", annotation=").append(annotation);
		buf.append(", euro_per_l=").append(euro_per_l);
		buf.append("}");
		return buf.toString();
	}

	public void setAmountString(String string) {
		this.amount = string == null ? null : new BigDecimal(string);
	}

	public String getAmountString() {
		return this.amount == null ? "" : this.amount.toPlainString();
	}

	public void setPriceString(String string) {
		this.price = string == null ? null : new BigDecimal(string);
	}

	public String getPriceString() {
		return this.price == null ? "" : this.price.toPlainString();
	}

	public void setBuydateString(String stringDate) {
		this.buydate = LocalDateTime.parse(stringDate, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
	}

	public String getBuydateString() {
		return this.buydate == null ? null : this.buydate.toString();
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the buydate
	 */
	public LocalDateTime getBuydate() {
		return buydate;
	}

	/**
	 * @param buydate the buydate to set
	 */
	public void setBuydate(LocalDateTime buydate) {
		this.buydate = buydate;
	}

	/**
	 * @return the fuel
	 */
	public String getFuel() {
		return fuel;
	}

	/**
	 * @param fuel the fuel to set
	 */
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the mileage
	 */
	public Integer getMileage() {
		return mileage;
	}

	/**
	 * @param mileage the mileage to set
	 */
	public void setMileage(Integer mileage) {
		this.mileage = mileage;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * @param provider the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * @return the annotation
	 */
	public String getAnnotation() {
		return annotation;
	}

	/**
	 * @param annotation the annotation to set
	 */
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	/**
	 * @return the euro_per_l
	 */
	public BigDecimal getEuro_per_l() {
		return euro_per_l;
	}

	/**
	 * @param euro_per_l the euro_per_l to set
	 */
	public void setEuro_per_l(BigDecimal euro_per_l) {
		this.euro_per_l = euro_per_l;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}
}
