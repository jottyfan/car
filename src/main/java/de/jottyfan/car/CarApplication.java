package de.jottyfan.car;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 
 * @author jotty
 * 
 */
@SpringBootApplication
@EnableTransactionManagement
public class CarApplication extends SpringBootServletInitializer {
  
	@Override
  protected SpringApplicationBuilder configure(
    SpringApplicationBuilder application) {
    return application.sources(CarApplication.class);
  }

	public static void main(String[] args) {
		SpringApplication.run(CarApplication.class, args);
	}

}
